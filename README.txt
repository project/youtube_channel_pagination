DESCRIPTION
------
This module creates a youtube channel video data in a block for the website.
With a given youtube channel user id or username you can access the latest videos
of the user. You can also set the height and width required for the
youtube channel.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/youtubechannelpagination

  * To submit bug reports and feature suggestions or to track changes:
    https://www.drupal.org/project/issues/youtubechannelpagination

    
INSTALLATION
------------
1. Extract the tar.gz into your 'modules' directory.
2. Enable the module at 'administer/modules'.


CONFIGURATION
-------------
 * Install the youtubechannelpagination module

 * Grab an API key. (details here: https://www.drupal.org/node/2474977#comment-9900267 ,https://console.developers.google.com/apis/dashboard)

 * Grab the channel ID from your youtube channel (open the page and look at the URL).
 
 * Configure the module and add the settings above from "admin/config/services/youtubechannelpagination".
 
 * Enable the youtubechannelpagination block.


MAINTAINERS
-----------
Current maintainers:
  * Balasaheb Bhise - https://www.drupal.org/u/balasahebbhise



